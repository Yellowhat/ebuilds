EAPI=8

inherit git-r3

DESCRIPTION="Simple terminal emulator for Wayland and X11 with OpenGL rendering and minimal dependencies."
HOMEPAGE="https://github.com/91861/wayst"
EGIT_REPO_URI="https://github.com/91861/wayst"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="
    dev-libs/libutf8proc
    media-libs/mesa
    media-libs/freetype
    media-libs/fontconfig
    x11-libs/libxkbcommon"

DEPEND="${RDEPEND}"

src_install() {
    dobin wayst
}

