EAPI=8

inherit font

DESCRIPTION="A legible monospace font"
HOMEPAGE="https://dtinth.github.io/comic-mono-font/"

SRC_URI="https://dtinth.github.io/comic-mono-font/ComicMono.ttf"
KEYWORDS="~amd64"
S="${WORKDIR}"

SLOT="0"

src_prepare() {
    default
    cp "${DISTDIR}"/ComicMono.ttf "${S}"/${P}.ttf || die
}

src_install() {
    FONT_SUFFIX=ttf font_src_install
    font_fontconfig
}
