EAPI=8

inherit git-r3

PYTHON_COMPAT=( python3_{9..11} )

DESCRIPTION="A simple console file manager written in python"
HOMEPAGE="https://gitlab.com/yellowhat-labs/sf"
EGIT_REPO_URI="https://gitlab.com/yellowhat-labs/${PN}"

LICENSE="GPLv3"
SLOT=0
KEYWORDS="~amd64"

src_install() {
    cp sf.py sf || die
    dobin sf
}
